console.log(document.getElementById("buttonMenu"));
var button = document.getElementById("buttonMenu");
document.getElementById("buttonMenu").classList.add("default-gap");
button.addEventListener("click", (event) => {
	var buttonMenu = document.getElementById("buttonMenu");
	if (!buttonMenu.classList.contains("gap-out")) {
		document.getElementById("second").classList.add("move-out");
		document.getElementById("buttonMenu").classList.add("gap-out");
		document.getElementById("third").classList.add("rotate-120-in");
		document.getElementById("first").classList.add("rotate-30-in");

		document.getElementById("third").classList.remove("rotate-120-out");
		document.getElementById("first").classList.remove("rotate-30-out");
		document.getElementById("second").classList.remove("move-in");
		document.getElementById("buttonMenu").classList.remove("gap-in");
		document.getElementById("buttonMenu").classList.remove("default-gap");
	} else {
		document.getElementById("third").classList.add("rotate-120-out");
		document.getElementById("first").classList.add("rotate-30-out");

		document.getElementById("buttonMenu").classList.add("gap-in");

		document.getElementById("second").classList.remove("move-out");
		document.getElementById("buttonMenu").classList.remove("gap-out");
		document.getElementById("third").classList.remove("rotate-120-in");
		document.getElementById("first").classList.remove("rotate-30-in");
		document.getElementById("second").classList.add("move-in");
	}
});
